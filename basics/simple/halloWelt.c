#include <stdio.h>
#define STR_END '\0'

int main()
{
  /* the simplest program - to test Makefile and DDD*/
  int index = -1;
  char lokalesArray[30] = "Hello World!!!!";
  char* zeigerAufZeichenkettenkonst = "Hallo Welt!";

  printf("%s\n", lokalesArray);

  for (index = 0; *zeigerAufZeichenkettenkonst != STR_END; index++)
    lokalesArray[index] = *zeigerAufZeichenkettenkonst++;

  lokalesArray[index] = STR_END;

  printf("\noder...\n\n%s\n", lokalesArray);

  return 0;
}
